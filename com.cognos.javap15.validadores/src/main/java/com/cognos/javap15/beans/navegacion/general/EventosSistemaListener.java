/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.beans.navegacion.general;

import javax.faces.application.Application;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.PostConstructApplicationEvent;
import javax.faces.event.PreDestroyApplicationEvent;
import javax.faces.event.SystemEvent;
import javax.faces.event.SystemEventListener;

/**
 *
 * @author JAVA
 */
public class EventosSistemaListener implements SystemEventListener {

    @Override
    public void processEvent(SystemEvent se) throws AbortProcessingException {
        if (se instanceof PostConstructApplicationEvent) {
            System.out.println("******** INCIANDO APLICACIÓN ********");
        } else if (se instanceof PreDestroyApplicationEvent) {
            System.out.println("******** FINALIZANDO APLICACIÓN ********");
        }
    }

    @Override
    public boolean isListenerForSource(Object o) {
        return (o instanceof Application);
    }

}
