/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.beans.navegacion.beans;

import com.cognos.javap15.beans.navegacion.model.Persona;
import com.cognos.javap15.beans.navegacion.utils.Cttes;
import java.util.List;
import java.util.Locale;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author JAVA
 */
@Named
@RequestScoped
public class IndexBean {
    
    @Inject
    Persona persona; //si no definimos ni public ni private, por defecto le pone package
    
    private List<String> errores;
    
    public String procesar(){
        System.out.println("Ingresando al metodo");
        validar();
        if(errores != null && errores.size() > 0){
            //return "error";
            return null;
        }else{
            return Cttes.perfilNav;
        }
    }
    
    private void validar(){
        errores = persona.validar();
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public List<String> getErrores() {
        return errores;
    }

    public void setErrores(List<String> errores) {
        this.errores = errores;
    }
    
    public void cambiarIdioma(){
        System.out.println("........... Ingresando al método cambiarIdioma ");
        FacesContext.getCurrentInstance().getViewRoot().setLocale(Locale.forLanguageTag("es"));
    }
    
    public void cambiarNick(ValueChangeEvent ve){
        //System.out.println("Ingresando a cambiarNick " + ve.getNewValue());
        
        /*
        //hacer simplemente esto parece que funcionaría pero el objeto persona aún no ha sido instanciada en este momento
        persona.setNick(persona.getNombre());
        */
        
        String valor = ve.getNewValue().toString();
        UIViewRoot uIViewRoot = FacesContext.getCurrentInstance().getViewRoot();
        HtmlInputText itxNick = (HtmlInputText) uIViewRoot.findComponent("frmPrincipal:itxNick");
        itxNick.setValue(valor);
        itxNick.setSubmittedValue(valor);
        //luego refrescamos la vista
        FacesContext.getCurrentInstance().renderResponse();
    }
    
    public void cambiarNickAjax(){
        System.out.println("........... Ingresando al método cambiarNickAjax ");
        persona.setNick(persona.getApellido());
    }
    
}
