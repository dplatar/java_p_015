/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.showcase.jsf.beans;

import com.google.common.io.Resources;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;

/**
 *
 * @author dplata
 */
@Named
@RequestScoped
public class SelectManyListboxBean implements Serializable {

    private List<SelectItem> paises;
    private String[] paisesSeleccionados;
    private String textoXHTML;
    private String textoJAVA;

    @PostConstruct
    public void init() {
        textoXHTML = obtenerContenido("selectmanylistboxxhtml.txt");
        textoJAVA = obtenerContenido("selectmanylistboxjava.txt");

        paises = new ArrayList<SelectItem>();
        paises.add(new SelectItem(1, "Argentina"));
        paises.add(new SelectItem(2, "Bolivia"));
        paises.add(new SelectItem(3, "Brasil"));
        paises.add(new SelectItem(4, "Colombia"));
        paises.add(new SelectItem(5, "Chile"));
        paises.add(new SelectItem(6, "Ecuador"));
        paises.add(new SelectItem(7, "Perú"));
        paises.add(new SelectItem(8, "Paraguay"));
        paises.add(new SelectItem(9, "Uruguay"));
        paises.add(new SelectItem(10, "Venezuela"));
    }

    public String obtenerContenido(String nombreArchivo) {
        try {
            URL url = Resources.getResource(nombreArchivo);
            String texto = Resources.toString(url, StandardCharsets.UTF_8);
            return texto;
        } catch (MalformedURLException ex) {
            System.out.println("URL malformada " + ex);
        } catch (IOException ex) {
            System.out.println("Error al leer el archivo " + ex);
        }
        return null;
    }

    public List<SelectItem> getPaises() {
        return paises;
    }

    public void setPaises(List<SelectItem> paises) {
        this.paises = paises;
    }

    public String[] getPaisesSeleccionados() {
        return paisesSeleccionados;
    }

    public void setPaisesSeleccionados(String[] paisesSeleccionados) {
        this.paisesSeleccionados = paisesSeleccionados;
    }

    public String getTextoXHTML() {
        return textoXHTML;
    }

    public void setTextoXHTML(String textoXHTML) {
        this.textoXHTML = textoXHTML;
    }

    public String getTextoJAVA() {
        return textoJAVA;
    }

    public void setTextoJAVA(String textoJAVA) {
        this.textoJAVA = textoJAVA;
    }

}
