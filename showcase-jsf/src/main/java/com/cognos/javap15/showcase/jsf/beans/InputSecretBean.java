/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.showcase.jsf.beans;

import com.google.common.io.Resources;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author JAVA
 */
@Named
@RequestScoped
public class InputSecretBean {

    private String password;
    private String textoXHTML;
    private String textoJAVA;

    @PostConstruct
    public void init() {
        textoXHTML = obtenerContenido("inputsecretxhtml.txt");
        textoJAVA = obtenerContenido("inputsecretjava.txt");
    }

    public String obtenerContenido(String nombreArchivo) {
        try {
            URL url = Resources.getResource(nombreArchivo);
            String texto = Resources.toString(url, StandardCharsets.UTF_8);
            return texto;
        } catch (MalformedURLException ex) {
            System.out.println("URL malformada " + ex);
        } catch (IOException ex) {
            System.out.println("Error al leer el archivo " + ex);
        }
        return null;
    }

    public String getTextoXHTML() {
        return textoXHTML;
    }

    public void setTextoXHTML(String textoXHTML) {
        this.textoXHTML = textoXHTML;
    }

    public String getTextoJAVA() {
        return textoJAVA;
    }

    public void setTextoJAVA(String textoJAVA) {
        this.textoJAVA = textoJAVA;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
