/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.showcase.jsf.beans;

import com.cognos.javap15.showcase.jsf.model.Persona;
import com.google.common.io.Resources;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author JAVA
 */
@Named
@RequestScoped
public class DataTableBean {

    private List<Persona> personas;
    private String textoXHTML;
    private String textoJAVA;
    private String datosPersona;

    @PostConstruct
    public void init() {
        textoXHTML = obtenerContenido("datatablexhtml.txt");
        textoJAVA = obtenerContenido("datatablejava.txt");

        personas = new ArrayList<>();
        personas.add(new Persona("Juan", "Perez", 25, "M"));
        personas.add(new Persona("Alberto", "Sanchez", 20, "M"));
        personas.add(new Persona("Carla", "Mendez", 25, "F"));
    }
    
    public void cargarPersona(Persona persona){
        datosPersona = persona.toString();
    }

    public String obtenerContenido(String nombreArchivo) {
        try {
            URL url = Resources.getResource(nombreArchivo);
            String texto = Resources.toString(url, StandardCharsets.UTF_8);
            return texto;
        } catch (MalformedURLException ex) {
            System.out.println("URL malformada " + ex);
        } catch (IOException ex) {
            System.out.println("Error al leer el archivo " + ex);
        }
        return null;
    }

    public String getTextoXHTML() {
        return textoXHTML;
    }

    public void setTextoXHTML(String textoXHTML) {
        this.textoXHTML = textoXHTML;
    }

    public String getTextoJAVA() {
        return textoJAVA;
    }

    public void setTextoJAVA(String textoJAVA) {
        this.textoJAVA = textoJAVA;
    }

    public List<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }

    public String getDatosPersona() {
        return datosPersona;
    }

    public void setDatosPersona(String datosPersona) {
        this.datosPersona = datosPersona;
    }
    
    

}
