/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.dagrey.sa.jsf.beans;

import bo.dagrey.sa.bl.ProfesorBL;
import bo.dagrey.sa.model.Profesor;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Diego Plata
 */
@Named
@RequestScoped
public class ProfesorBean implements Serializable {

    @Inject
    private ProfesorBL profesorBL;

    private List<Profesor> profesores;
    private Profesor profesorNuevo;

    public void actualizarProfesores() {
        profesores = profesorBL.listarProfesores();
    }

    @PostConstruct
    public void init() {
        actualizarProfesores();
        profesorNuevo = new Profesor();
    }

    public void registrarProfesor() {
        profesorBL.registrarProfesor(profesorNuevo);
        profesorNuevo = new Profesor();
        actualizarProfesores();
    }

    public void seleccionarProfesor(Profesor profesor) {
        this.profesorNuevo = profesor;
        System.out.println("ProfesorBean: Profesor seleccionado. rfc = " + profesor.getRfc());
    }

    public void modificarProfesor() {
        System.out.println("ProfesorBean: modificarProfesor. rfc = " + profesorNuevo.getRfc());
        profesorBL.modificarProfesor(profesorNuevo);
        profesorNuevo = new Profesor();
        actualizarProfesores();
    }

    public void eliminarProfesor(Profesor profesor) {
        System.out.println("ProfesorBean: eliminarProfesor. rfc = " + profesor.getRfc());
        profesorBL.eliminarProfesor(profesor);
        actualizarProfesores();
    }

    public List<Profesor> getProfesores() {
        return profesores;
    }

    public void setProfesores(List<Profesor> profesores) {
        this.profesores = profesores;
    }

    public Profesor getProfesorNuevo() {
        return profesorNuevo;
    }

    public void setProfesorNuevo(Profesor profesorNuevo) {
        this.profesorNuevo = profesorNuevo;
    }

}
