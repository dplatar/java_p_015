/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.dagrey.sa.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Diego Plata
 */
@Entity
@Table(name = "profesor")
@NamedQueries({
    @NamedQuery(name = Profesor.LISTAR_PROFESORES, query = "SELECT p FROM Profesor p")
})
public class Profesor implements Serializable {

    public final static String LISTAR_PROFESORES = "Profesor.ListarProfesores";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long rfc;

    @Basic
    @Column(name = "nombre")
    private String nombre;

    @Basic
    @Column(name = "ap_paterno")
    private String apellidoPaterno;

    @Basic
    @Column(name = "ap_materno")
    private String apellidoMaterno;

    @Basic
    @Column(name = "direccion")
    private String direccion;

    @Basic
    @Column(name = "telefono")
    private String telefono;

    public Profesor() {
    }

    public Profesor(Long rfc, String nombre, String apellidoPaterno, String apellidoMaterno, String direccion, String telefono) {
        this.rfc = rfc;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public Long getRfc() {
        return rfc;
    }

    public void setRfc(Long rfc) {
        this.rfc = rfc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

}
