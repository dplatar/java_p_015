/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.dagrey.sa.bl.impl;

import bo.dagrey.sa.bl.ProfesorBL;
import bo.dagrey.sa.dao.ProfesorDAO;
import bo.dagrey.sa.model.Profesor;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;


@Stateless
public class ProfesorBLImpl implements ProfesorBL {

    
    @Inject
    private ProfesorDAO profesorDAO;
    
    @Override
    public List<Profesor> listarProfesores() {
        return profesorDAO.listarProfesores();
    }

    @Override
    public Profesor registrarProfesor(Profesor profesor) {
        return profesorDAO.registrarProfesor(profesor);
    }

    @Override
    public Profesor modificarProfesor(Profesor profesor) {
        return profesorDAO.modificarProfesor(profesor);
    }

    @Override
    public Profesor eliminarProfesor(Profesor profesor) {
        return profesorDAO.eliminarProfesor(profesor);
    }
    
}
