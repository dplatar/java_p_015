/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.dagrey.sa.bl;

import bo.dagrey.sa.model.Profesor;
import java.util.List;

/**
 *
 * @author Diego Plata
 */
public interface ProfesorBL {
    
    List<Profesor> listarProfesores();
    
    Profesor registrarProfesor(Profesor profesor);
    
    Profesor modificarProfesor(Profesor profesor);
    
    Profesor eliminarProfesor(Profesor profesor);
}
