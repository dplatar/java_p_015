/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.dagrey.sa.dao;

import bo.dagrey.sa.model.Profesor;
import java.util.List;

/**
 *
 * @author Diego Plata
 */
public interface ProfesorDAO {
    
    List<Profesor> listarProfesores();
    
    Profesor registrarProfesor(Profesor profesor);
    
    Profesor modificarProfesor(Profesor profesor);
    
    Profesor eliminarProfesor(Profesor profesor);
    
}
