/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.dagrey.sa.dao.impl;

import bo.dagrey.sa.dao.ProfesorDAO;
import bo.dagrey.sa.model.Profesor;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ProfesorDAOImpl implements ProfesorDAO {
    
    @PersistenceContext(unitName = "sis-acad")
    EntityManager em;

    @Override
    public List<Profesor> listarProfesores() {
        return em.createNamedQuery(Profesor.LISTAR_PROFESORES).getResultList();
    }

    @Override
    public Profesor registrarProfesor(Profesor profesor) {
        em.persist(profesor);
        System.out.println("ProfesorDAOImpl: profesor registrado");
        return profesor;
    }

    @Override
    public Profesor modificarProfesor(Profesor profesor) {
        Profesor p = em.find(Profesor.class, profesor.getRfc());
        p.setNombre(profesor.getNombre());
        p.setApellidoPaterno(profesor.getApellidoPaterno());
        p.setApellidoMaterno(profesor.getApellidoMaterno());
        p.setDireccion(profesor.getDireccion());
        p.setTelefono(profesor.getTelefono());
        profesor = em.merge(p);
        System.out.println("ProfesorDAOImpl: profesor modificado");
        return profesor;
    }

    @Override
    public Profesor eliminarProfesor(Profesor profesor) {
        Profesor p = em.find(Profesor.class, profesor.getRfc());
        em.remove(p);
        System.out.println("ProfesorDAOImpl: profesor eliminado");
        return profesor;
    }
    
}
