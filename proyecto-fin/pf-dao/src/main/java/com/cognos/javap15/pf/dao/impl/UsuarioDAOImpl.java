/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.dao.impl;

import com.cognos.javap15.pf.dao.UsuarioDAO;
import com.cognos.javap15.pf.model.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author JAVA
 */
@Stateless //Define esta clase como EJB
public class UsuarioDAOImpl implements UsuarioDAO{
    
    @PersistenceContext(unitName = "proy-fin")
    EntityManager em;
    
    @Override
    public List<Usuario> listarUsuarios() {
        return em.createNamedQuery(Usuario.LISTAR_USUARIOS).getResultList();
    }

    @Override
    public Usuario registrarUsuario(Usuario usuario) {
        em.persist(usuario);
        return usuario;
    }

    @Override
    public Usuario actualizarUsuario(Usuario usuario) {
        System.out.println("Obtener usuario " + usuario.getId());
        Usuario u = em.find(Usuario.class, usuario.getId());
        u.setNombre(usuario.getNombre());
        u.setApellido(usuario.getApellido());
        u.setLogin(usuario.getLogin());
        u.setEmail(usuario.getEmail());
        usuario = em.merge(u);
        return usuario;
    }

    @Override
    public Usuario eliminarUsuario(Usuario usuario) {
        Usuario u = em.find(Usuario.class, usuario.getId());
        em.merge(u);
        return usuario;
    }
    
}
