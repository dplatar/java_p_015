/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.dao;

import com.cognos.javap15.pf.model.Usuario;
import java.util.List;

/**
 *
 * @author JAVA
 */
public interface UsuarioDAO {
    
    List<Usuario> listarUsuarios();
    
    Usuario registrarUsuario(Usuario usuario);
    
    Usuario actualizarUsuario(Usuario usuario);
    
    Usuario eliminarUsuario(Usuario usuario);
    
}
