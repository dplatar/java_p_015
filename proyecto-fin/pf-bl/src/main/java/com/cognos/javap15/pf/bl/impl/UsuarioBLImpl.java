/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.bl.impl;

import com.cognos.javap15.pf.bl.UsuarioBL;
import com.cognos.javap15.pf.dao.UsuarioDAO;
import com.cognos.javap15.pf.model.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author JAVA
 */
@Stateless
public class UsuarioBLImpl implements UsuarioBL{
    
    @Inject
    private UsuarioDAO usuarioDAO;

    @Override
    public List<Usuario> listarUsuarios() {
        return usuarioDAO.listarUsuarios();
    }

    @Override
    public Usuario registrarUsuario(Usuario usuario) {
        return usuarioDAO.registrarUsuario(usuario);
    }

    @Override
    public Usuario actualizarUsuario(Usuario usuario) {
        return usuarioDAO.actualizarUsuario(usuario);
    }

    @Override
    public Usuario eliminarUsuario(Usuario usuario) {
        return usuarioDAO.eliminarUsuario(usuario);
    }
    
    
    
}
