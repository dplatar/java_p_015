/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.jsf.beans;

import com.cognos.javap15.pf.bl.UsuarioBL;
import com.cognos.javap15.pf.model.Usuario;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author JAVA
 */
@Named
@RequestScoped
public class UsuarioBean {
    
    @Inject
    private UsuarioBL usuarioBL;
    
    private List<Usuario> usuarios;
    
    private Usuario usuarioNuevo;
    
    @PostConstruct
    public void init(){
        actualizarUsuarios();
        usuarioNuevo = new Usuario();
    }
    
    public void actualizarUsuarios(){
        usuarios = usuarioBL.listarUsuarios();
    }

    /*public UsuarioBL getUsuarioBL() {
        return usuarioBL;
    }

    public void setUsuarioBL(UsuarioBL usuarioBL) {
        this.usuarioBL = usuarioBL;
    }*/

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public UsuarioBL getUsuarioBL() {
        return usuarioBL;
    }

    public void setUsuarioBL(UsuarioBL usuarioBL) {
        this.usuarioBL = usuarioBL;
    }

    public Usuario getUsuarioNuevo() {
        return usuarioNuevo;
    }

    public void setUsuarioNuevo(Usuario usuarioNuevo) {
        this.usuarioNuevo = usuarioNuevo;
    }
    
    public void registrarUsuario(){
        usuarioBL.registrarUsuario(usuarioNuevo);
        actualizarUsuarios();
    }
    
    public void seleccionarUsuario(Usuario usuario){
        usuarioNuevo = usuario;
    }
    
    public void actualizarUsuario(){
        usuarioBL.actualizarUsuario(usuarioNuevo);
        usuarioNuevo = new Usuario();
        actualizarUsuarios();
    }
    
    public void eliminarUsuario(Usuario usuario){
        usuarioBL.eliminarUsuario(usuario);
        actualizarUsuarios();
        System.out.println("Usuario eliminado. id " + usuario.getId());
    }
    
}
