/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.examen1.beans;

import com.cognos.javap15.examen1.model.Libro;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author JAVA
 */
@Named
@RequestScoped
public class RegistroBean {
    
    @Inject
    Libro libro;
    
    private String mensaje;
    
    public void registrar(){
        System.out.println("Ingresando al método registrar");
        mensaje = "Libro registrado: " + libro.toString();
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    
    
}
