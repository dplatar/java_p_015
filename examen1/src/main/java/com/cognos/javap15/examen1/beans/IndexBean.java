/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.examen1.beans;

import com.cognos.javap15.examen1.model.Usuario;
import java.util.Locale;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author JAVA
 */
@Named
@RequestScoped
public class IndexBean {
    
    @Inject
    Usuario usuario;
    
    public String procesar(){
        System.out.println("Ingresando al método procesar");
        if(usuario.getUser().equals("cuenta@mail.com") && usuario.getPassword().equals("password")){
            //return Cttes.perfilNav;
            return "registro";
        }else{
            return "error";
        }
    }
    
    public void cambiarIdioma(){
        System.out.println("........... Ingresando al método cambiarIdioma ");
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        if(locale.toString().equals("es")){
            FacesContext.getCurrentInstance().getViewRoot().setLocale(Locale.forLanguageTag("en"));
        }else{
            FacesContext.getCurrentInstance().getViewRoot().setLocale(Locale.forLanguageTag("es"));
        }
    }
    
}
