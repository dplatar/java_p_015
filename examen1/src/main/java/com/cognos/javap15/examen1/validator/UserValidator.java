/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.examen1.validator;

import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author JAVA
 */
@FacesValidator("userValidator")
public class UserValidator implements Validator<String> {

    @Override
    public void validate(FacesContext fc, UIComponent uic, String t) throws ValidatorException {
        /*String zeroTo255
            = "([01]?[0-9]{1,2}|2[0-4][0-9]|25[0-5])";
        String ip_regexp
            = zeroTo255 + "\\." + zeroTo255 + "\\."
            + zeroTo255 + "\\." + zeroTo255;
        Pattern ip_pattern
            = Pattern.compile(ip_regexp);
        //if(!ip_pattern.matcher(t).matches()){
        if(!Pattern.matches(ip_regexp,t)){
            FacesMessage msg = new FacesMessage("Error IP","Formato de IPv4 incorrecto");
            throw new ValidatorException(msg);
        }*/
        if(!t.contains("@") && !t.contains(".")){
            FacesMessage msg = new FacesMessage("Error IP","Formato de e-mail incorrecto");
            throw new ValidatorException(msg);
        }
    }
    
}
