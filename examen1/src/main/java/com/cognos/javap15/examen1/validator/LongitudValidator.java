/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.examen1.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author JAVA
 */
@FacesValidator("longitudValidator")
public class LongitudValidator implements Validator<String> {

    @Override
    public void validate(FacesContext fc, UIComponent uic, String t) throws ValidatorException {
        if(t.length()<3){
            FacesMessage msg = new FacesMessage("Error IP","Longitud debe ser mayor a 3");
            throw new ValidatorException(msg);
        }
    }
    
}
